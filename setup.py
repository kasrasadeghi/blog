import os
from subprocess import check_output
from shutil import copy2, rmtree

def system(cmd):
    check_output(cmd.split())

# tells you whether a filename is a blog entry
def bloggy(filename):
    with open(filename) as f:
        content = f.read()
    return '=> blog-entry' in content

try:
    rmtree('entries')
except:
    pass

os.mkdir('entries')

entries_dir = os.getcwd() + '/entries'

with open ('notes_location.txt') as f:
    content = f.read().strip()

print(content)
os.chdir(content)
print(os.listdir())

l = list(filter(bloggy, os.listdir()))
print(l)

for fn in l:
    copy2(fn, entries_dir)
