from pprint import pprint

import sys
import html

def parse_entry(entry_name):
  with open('entries/' + entry_name) as f:
      lines = f.readlines()

  # push-down automata
  tags = []
  headings = [{'title': 'root', 'content': []}]
  lines = map(lambda l: l[:-1], lines)

  # get headings
  for l in lines:
    if '=> blog-entry' in l:
      continue
    if l.startswith('=>'):
      tags.append(l[3:].strip())
      continue
      
    if l.startswith('*'):
      headings.append({'title': l[1:].strip(), 'content': []})
    else:
      curr_heading = headings[-1]
      curr_content = curr_heading['content']
      curr_content.append(l[:])
    
  # delete empty root
  if not "".join(headings[0]['content']).strip():
    del headings[0]
  
  # make paragraphs
  state = 'normal' # normal | source
  for h in headings:
    paragraphs = [[]]
    for l in h['content']:

      # source parsing
      if '#+BEGIN_SRC' in l:
        paragraphs.append({'source': l[len('#+BEGIN_SRC') + 1:], 'content': []})
        state = 'source'
        continue
      elif state == 'source':
        if '#+END_SRC' in l:
          state = 'normal'
        else:
          paragraphs[-1]['content'].append(l)
        continue
        
      if state == 'normal':
        # if the line is empty, we're starting a new paragraph
        if l == '':
          # unless we haven't added anything to the current one
          if paragraphs[-1] == []:
            continue
          else:
            paragraphs.append([])
        else:
          paragraphs[-1].append(l)
    h['paragraphs'] = paragraphs
  
  # remove empty paragraphs
  for h in headings:
    h['paragraphs'] = [p for p in h['paragraphs'] if p]
  
  return {'entry_name': entry_name, 'tags': tags, 'headings': headings}

def print_entry(entry):
  for heading in entry['headings']:
    print(heading['title'])
    for l in heading['content']:
      print(l)
    print()

def html_entry(entry):
  start = f'''
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{entry['entry_name']}</title>
</head>
<body>
<div style="width:80%; margin-top: 5%; margin-left:10%">
'''.strip()

  end = '</div>\n</body>\n</html>'

  print(start)
  
  for heading in entry['headings']:
    print(f"<h1>{heading['title']}</h1>")
    for p in heading['paragraphs']:
      if type(p).__name__ == 'dict' and 'source' in p:
        print('<pre>')
        print(html.escape("\n".join(p['content'])))
        print('</pre>')
      else:
        print("<p>" + '\n'.join(p) + "</p>")
    print()
      
  print(end)


entry = parse_entry('super-aliases.org')

# pprint(entry)

cache = sys.stdout
with open('output.html', 'w') as f:
  sys.stdout = f
  html_entry(entry)
sys.stdout = cache